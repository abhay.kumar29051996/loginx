import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Injectable, ElementRef, ViewChild } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from './apiService';
/**
 * Node for to-do item
 */
export class TodoItemNode {
  children: TodoItemNode[];
  label: string;
  pid:Number
}

/** Flat to-do item node with expandable and level information */
export class TodoItemFlatNode {
  label: string;
  level: number;
  pid:Number;
  expandable: boolean;
}

/**
 * The Json object for to-do list data.
 */
const TREE_DATA = {
};

/**
 * Checklist database, it can build a tree structured Json object.
 * Each node in Json object represents a to-do item or a category.
 * If a node is a category, it has children items and new items can be added under the category.
 */
@Injectable()
export class ChecklistDatabase {
  dataChange = new BehaviorSubject<TodoItemNode[]>([]);

  get data(): TodoItemNode[] { return this.dataChange.value; }

  constructor(private apiService:ApiService) {
    this.initialize();
    this.getData()
  }
  getData(){
    this.apiService.get().subscribe(res=>{
      var data1= this.convertToTree(res);
       const data = this.buildFileTree(data1, 0);

      // Notify the change.
      this.dataChange.next(data);
    
  
    })
  }
  convertToTree(data){
    data =data.filter((value, index, self) =>
  index === self.findIndex((t) => (
    t.client_company_name == value.client_company_name && t.server_group_name == value.server_group_name
    &&  t.server_name == value.server_name && t.instance_name == value.instance_name && t.database_name == value.database_name
  ))
)

    let b = [];
    var database_name=[];
data.map(item => {
 
  let obj = b.findIndex(o => {
  var de= o.label.localeCompare(item.client_company_name)
   return de==0?true:false
  })

var ind=0;
  if (obj == -1) {
  
    let d = {
      label: item.client_company_name,
      data:item.client_company_name,
      children: [],
  
    }
    d.children.push({
      label:item.server_group_name,
      data:item.server_group_name,
    
      children:[{
        label:item.server_name,
        data:item.server_name,
     
        children:[{
          label:item.instance_name,
          data:item.instance_name,
         
          children:[{
            label:item.database_name,
            data:item.database_name,
           
          }]
        }]
      }]
    })
    b.push(d)


  }
})

  for (let i=0;i<b.length;i++){
    let c=b[i].children;
   

   
    for (let j=0;j<c.length;j++){
        
       data.map(o => {
      
        if(o.client_company_name==b[i].label){
    
        if(o.server_group_name==b[i].children[j].label){
          
        }
        else{
        
          let obj1 = c.findIndex(k => {
          
            var de= k.label.localeCompare(o.server_group_name)
             return de==0?true:false
            })
          
            if(obj1==-1){
           
              c.push({
                label:o.server_group_name,
                data:o.server_group_name,
                pid:i+1,
                children:[{
                  label:o.server_name,
                  data:o.server_name,
                  pid:i+1,
                  children:[{
                    label:o.instance_name,
                    data:o.instance_name,
                    pid:i+1,
                    children:[{
                      label:o.database_name,
                      data:o.database_name,
                      pid:i+1
                    }]
                  }]
                }]
              })
            }
        }
        }
         var de= o.server_group_name.localeCompare(c[j].label)
          return de==0?true:false
         })

      let d=b[i].children[j].children
    

      for (let k=0;k<d.length;k++){
        
        data.map(o => {
      
          if(o.client_company_name==b[i].label){
          if(o.server_group_name==b[i].children[j].label && d.length>0){
            
           if(o.server_name==d[k].label)
           {
           
          }
          else{
           
            let obj1 = d.findIndex(k => {
              var de= k.label.localeCompare(o.server_name)
               return de==0?true:false
              })
            
              if(obj1==-1){
            
                d.push({
                  label:o.server_name,
                  data:o.server_name,
                  pid:i+1
                })
              }
          }
       
          }
   
        }
         
           })
     
           let e=d[k].children;
          
        for (let l=0;l<e.length;l++){
        
          data.findIndex(o => {
      
            if(o.client_company_name==b[i].label){
            if(o.server_group_name==b[i].children[j].label){
             if(o.server_name==d[k].label)
             {
              if(o.instance_name==e[k].label)
              {
              
             }
             else{
           
              let obj1 = e.findIndex(k => {
                var de= k.label.localeCompare(o.instance_name)
                 return de==0?true:false
                })
                if(obj1==-1){
                  e.push({
                    label:o.instance_name,
                    data:o.instance_name,
                    pid:i+1
                  })
                }
             }


            }
            
         
            }
          }
         
             })
             
             let f=e[l].children;
          for (let m=0;m<f.length;m++){
            data.findIndex(o => {
       
              if(o.client_company_name==b[i].label){
          
              if(o.server_group_name==b[i].children[j].label || o.server_group_name == ''){
               
               if(o.server_name==d[k].label)
               {
               
                if(o.instance_name==e[k].label)
                {
               

                  if(o.database_name==f[m].label)
                  {
                   
                 
                 }
                 else{
                 
                  let obj1 = f.findIndex(k => {
                    var de= k.label.localeCompare(o.database_name)
                     return de==0?true:false
                    })
                  
                    if(obj1==-1){
                    
                      f.push({
                        label:o.database_name,
                        data:o.database_name,
                        pid:i+1
                      })
                    }
                 }
               

               }
              
  
  
              }
              
           
              }
            }
           
               })
          }
        }
      }
    }


  }


return b;
  }

  initialize() {
    // Build the tree nodes from Json object. The result is a list of `TodoItemNode` with nested
    //     file node as children.
    const data = this.buildFileTree(TREE_DATA, 0);

    // Notify the change.
    this.dataChange.next(data);
  }

  /**
   * Build the file structure tree. The `value` is the Json object, or a sub-tree of a Json object.
   * The return value is the list of `TodoItemNode`.
   */
  buildFileTree(obj: object, level: number): TodoItemNode[] {
    return Object.keys(obj).reduce<TodoItemNode[]>((accumulator, key) => {
      const value = obj[key].label;
      const node = new TodoItemNode();
      node.label = obj[key].label;

      if (value != null) {
        if (typeof obj[key].children === 'object') {
          node.children = this.buildFileTree(obj[key].children, level + 1);
        } else {
          node.label = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }

  /** Add an item to to-do list */
  insertItem(parent: TodoItemNode, name: string): TodoItemNode {
    if (!parent.children) {
      parent.children = [];
    }
    const newItem = { label: name } as TodoItemNode;
    parent.children.push(newItem);
    this.dataChange.next(this.data);
    return newItem;
  }

  insertItemAbove(node: TodoItemNode, name: string): TodoItemNode {
    const parentNode = this.getParentFromNodes(node);
    const newItem = { label: name } as TodoItemNode;
    if (parentNode != null) {
      parentNode.children.splice(parentNode.children.indexOf(node), 0, newItem);
    } else {
      this.data.splice(this.data.indexOf(node), 0, newItem);
    }
    this.dataChange.next(this.data);
    return newItem;
  }

  insertItemBelow(node: TodoItemNode, name: string): TodoItemNode {
    const parentNode = this.getParentFromNodes(node);
    const newItem = { label: name } as TodoItemNode;
    if (parentNode != null) {
      parentNode.children.splice(parentNode.children.indexOf(node) + 1, 0, newItem);
    } else {
      this.data.splice(this.data.indexOf(node) + 1, 0, newItem);
    }
    this.dataChange.next(this.data);
    return newItem;
  }

  getParentFromNodes(node: TodoItemNode): TodoItemNode {
    for (let i = 0; i < this.data.length; ++i) {
      const currentRoot = this.data[i];
      const parent = this.getParent(currentRoot, node);
      if (parent != null) {
        return parent;
      }
    }
    return null;
  }

  getParent(currentRoot: TodoItemNode, node: TodoItemNode): TodoItemNode {
    if (currentRoot.children && currentRoot.children.length > 0) {
      for (let i = 0; i < currentRoot.children.length; ++i) {
        const child = currentRoot.children[i];
        if (child === node) {
          return currentRoot;
        } else if (child.children && child.children.length > 0) {
          const parent = this.getParent(child, node);
          if (parent != null) {
            return parent;
          }
        }
      }
    }
    return null;
  }

  updateItem(node: TodoItemNode, name: string) {
    node.label = name;
    this.dataChange.next(this.data);
  }

  deleteItem(node: TodoItemNode) {
    this.deleteNode(this.data, node);
    this.dataChange.next(this.data);
  }

  copyPasteItem(from: TodoItemNode, to: TodoItemNode): TodoItemNode {
    const newItem = this.insertItem(to, from.label);
    if (from.children) {
      from.children.forEach(child => {
        this.copyPasteItem(child, newItem);
      });
    }
    return newItem;
  }

  copyPasteItemAbove(from: TodoItemNode, to: TodoItemNode): TodoItemNode {
    const newItem = this.insertItemAbove(to, from.label);
    if (from.children) {
      from.children.forEach(child => {
        this.copyPasteItem(child, newItem);
      });
    }
    return newItem;
  }

  copyPasteItemBelow(from: TodoItemNode, to: TodoItemNode): TodoItemNode {
    const newItem = this.insertItemBelow(to, from.label);
    if (from.children) {
      from.children.forEach(child => {
        this.copyPasteItem(child, newItem);
      });
    }
    return newItem;
  }

  deleteNode(nodes: TodoItemNode[], nodeToDelete: TodoItemNode) {
    const index = nodes.indexOf(nodeToDelete, 0);
    if (index > -1) {
      nodes.splice(index, 1);
    } else {
      nodes.forEach(node => {
        if (node.children && node.children.length > 0) {
          this.deleteNode(node.children, nodeToDelete);
        }
      });
    }
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ChecklistDatabase]
})
export class AppComponent {
  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<TodoItemFlatNode, TodoItemNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<TodoItemNode, TodoItemFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: TodoItemFlatNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<TodoItemFlatNode>;

  treeFlattener: MatTreeFlattener<TodoItemNode, TodoItemFlatNode>;

  dataSource: MatTreeFlatDataSource<TodoItemNode, TodoItemFlatNode>;

  /** The selection for checklist */
  checklistSelection = new SelectionModel<TodoItemFlatNode>(true /* multiple */);

  /* Drag and drop */
  dragNode: any;
  dragNodeExpandOverWaitTimeMs = 300;
  dragNodeExpandOverNode: any;
  dragNodeExpandOverTime: number;
  dragNodeExpandOverArea: string;
  @ViewChild('emptyItem') emptyItem: ElementRef;

  constructor(private database: ChecklistDatabase,private apiService:ApiService) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<TodoItemFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    database.dataChange.subscribe(data => {
      this.dataSource.data = [];
      this.dataSource.data = data;
    });
    //this.getData()
  }
  

  getLevel = (node: TodoItemFlatNode) => node.level;

  isExpandable = (node: TodoItemFlatNode) => node.expandable;

  getChildren = (node: TodoItemNode): TodoItemNode[] => node.children;

  hasChild = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: TodoItemFlatNode) => _nodeData.label === '';

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: TodoItemNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.label === node.label
      ? existingNode
      : new TodoItemFlatNode();
    flatNode.label = node.label;
    flatNode.level = level;
    flatNode.expandable = (node.children && node.children.length > 0);
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  }

  /** Whether all the descendants of the node are selected */
  descendantsAllSelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    return descendants.every(child => this.checklistSelection.isSelected(child));
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: TodoItemFlatNode): boolean {
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some(child => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  todoItemSelectionToggle(node: TodoItemFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);
  }

  /** Select the category so we can insert the new item. */
  addNewItem(node: TodoItemFlatNode) {
    const parentNode = this.flatNodeMap.get(node);
    this.database.insertItem(parentNode, '');
    this.treeControl.expand(node);
  }

  /** Save the node to database */
  saveNode(node: TodoItemFlatNode, itemValue: string) {
    const nestedNode = this.flatNodeMap.get(node);
    this.database.updateItem(nestedNode, itemValue);
  }

  handleDragStart(event, node) {
    // Required by Firefox (https://stackoverflow.com/questions/19055264/why-doesnt-html5-drag-and-drop-work-in-firefox)
    event.dataTransfer.setData('foo', 'bar');
    event.dataTransfer.setDragImage(this.emptyItem.nativeElement, 0, 0);
    this.dragNode = node;
    this.treeControl.collapse(node);
  }

  handleDragOver(event, node) {
    event.preventDefault();

    // Handle node expand
    if (node === this.dragNodeExpandOverNode) {
      if (this.dragNode !== node && !this.treeControl.isExpanded(node)) {
        if ((new Date().getTime() - this.dragNodeExpandOverTime) > this.dragNodeExpandOverWaitTimeMs) {
          this.treeControl.expand(node);
        }
      }
    } else {
      this.dragNodeExpandOverNode = node;
      this.dragNodeExpandOverTime = new Date().getTime();
    }

    // Handle drag area
    const percentageX = event.offsetX / event.target.clientWidth;
    const percentageY = event.offsetY / event.target.clientHeight;
    if (percentageY < 0.25) {
      this.dragNodeExpandOverArea = 'above';
    } else if (percentageY > 0.75) {
      this.dragNodeExpandOverArea = 'below';
    } else {
      this.dragNodeExpandOverArea = 'center';
    }
  }
  expandParents(node: TodoItemFlatNode) {
    const parent = this.getParent(node);
    this.treeControl.expand(parent);
    if (parent && parent.level > 0) {
      this.expandParents(parent);
    }
  }
  /**
   * Iterate over each node in reverse order and return the first node that has a lower level than the passed node.
   */
  getParent(node: TodoItemFlatNode) {
    const { treeControl } = this;
    const currentLevel = treeControl.getLevel(node);
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = treeControl.dataNodes[i];
      if (treeControl.getLevel(currentNode) ==0) {
        return currentNode;
      }
    }
  }

  handleDrop(event, node) {
    event.preventDefault();
  
    if (node !== this.dragNode &&node.level==this.dragNode.level && node.level==2 && this.getParent(node).label===this.getParent(this.dragNode).label) {
     
      let newItem: TodoItemNode;
 
        newItem = this.database.copyPasteItemBelow(this.flatNodeMap.get(this.dragNode), this.flatNodeMap.get(node));
      
      this.database.deleteItem(this.flatNodeMap.get(this.dragNode));
      this.treeControl.expandDescendants(this.nestedNodeMap.get(newItem));
    }
    this.dragNode = null;
    this.dragNodeExpandOverNode = null;
    this.dragNodeExpandOverTime = 0;
  }

  handleDragEnd(event) {
    this.dragNode = null;
    this.dragNodeExpandOverNode = null;
    this.dragNodeExpandOverTime = 0;
  }
}
